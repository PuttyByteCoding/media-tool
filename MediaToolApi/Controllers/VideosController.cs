using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediaToolLib.Database;
using MediaToolLib.Ingestion;
using Nest;
using Ingest = MediaToolLib.Ingestion.Ingest;

namespace MediaToolApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VideosController : ControllerBase
    {
        // GET: api/Videos
        [HttpGet]
        public object GetVideos([FromQuery] int? count)
        {
            var mediaDB = new MediaDb();
            var results = MediaDb.GrabVideos(count);
            return results;
        }

        // GET: api/Videos/0b0ea08240fe99b2642532ef8b5f12ec
        [HttpGet("{md5hash}", Name = "Get")]
        public object GetMd5FromPath([FromRoute] string md5hash)
        {
            var mediaDB = new MediaDb();
            var results = MediaDb.GrabMediaByMd5(md5hash);
            return results;
        }
        
        // POST: api/Videos
        [HttpPost]
        public async Task<IActionResult> Post(IFormFile fileToProcess)
        {
            string uploadDir = "/Volumes/RAID/__TEMP/mediatool_samples/Uploads";
            Directory.CreateDirectory(uploadDir);
            if (fileToProcess.Length > 0)
            {
                string fileUploadPath = Path.Combine(uploadDir, fileToProcess.FileName);
                using (Stream fileStream = new FileStream(fileUploadPath, FileMode.Create, FileAccess.Write))
                {
                    await fileToProcess.CopyToAsync(fileStream);
                }
            }
            
            // TODO: Implement MD5 calc as part of destination path
            // TODO: Determine where md5 calc should live?  Used in Media Model, and here.
            
            var pathToFile = Path.Combine(uploadDir, fileToProcess.FileName);
            Ingest.IngestMedia(pathToFile);
            return Ok($"File received {fileToProcess.FileName}.  Size: {fileToProcess.Length}");
        }
        
    }
}

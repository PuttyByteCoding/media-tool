using Microsoft.EntityFrameworkCore;
using MediaToolLib.Models;
using Microsoft.Extensions.FileProviders;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddDbContext<MediaContext>(opt =>
    opt.UseInMemoryDatabase("MediaList"));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();
app.UseCors();

// app.UseStaticFiles(new StaticFileOptions
// {
//     FileProvider = new PhysicalFileProvider(builder.Configuration["LocalMediaFolder"]!),
//     RequestPath = "/media",
// });

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
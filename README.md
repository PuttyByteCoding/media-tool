# Media Tool
* Create a web app that can play Video and Audio from concerts (legal recordings only)
* This is a personal project to help practice csharp things.
* Right now media = audio and video.  Photos are not processed/displayed.

## List of features to consider
* ingest concert videos and audio from a local folder
* Validate Checksums
* Support Audio and Video
* Support searching tags
* Read all Media metadata
	* Artist
	* Album
	* Recorded date
	* Venue, city, state, etc
* Read all File Metadata
	* File Size
	* File Format - SHN, FLAC, Mp4, etc
	* File path
	* bitrate
	* checksum
* Support searching all metadata (both File and Media)
* Support modifying all metadata (both File and Media) 
* Play in a web browser
* Identify files that might be similar
* Compare two files that may be similar
* Generate Previews for video
* Split into multiple files
	* If a song has 2 that flow into each other, split to two tracks
* Normalize metadata
	* Song name (“Pig” vs “Don’t Burn the Pig)
	* Venue names (“Nissan Pavilion” vs “Jiffy Lube Live)
* Create a playlist by any tag
	* All recordings of a particular song
	* All songs with a specific guest(s)
* Stats from metadata
	* see antsmarching.org
* Normalize Audio for a playlist

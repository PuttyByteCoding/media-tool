using System.Text.Json;
using MediaToolLib.Models;
using MediaToolLib.Database;

namespace MediaToolLib.Ingestion;

public static class Ingest
{
    public static void IngestFromJsonFile(string jsonFilePath)
    {
        string jsonString = File.ReadAllText(jsonFilePath);
        List<Media> mediaList = new List<Media>();
        mediaList = JsonSerializer.Deserialize<List<Media>>(jsonString);
        foreach (var med in mediaList)
        {
            MediaDb.AddMedia(med);
        }
    }

    public static void IngestFromFolder(string folderPath)
    {
        int fileCountTotal = GetFileCount(folderPath);
        int fileCountProcessed = 0;
        var filenamesToSkip = new List<string>();
        filenamesToSkip.Add(".DS_Store");
        
        DirectoryInfo mediaDir = new DirectoryInfo(folderPath);
        FileInfo[] mediaFiles = mediaDir.GetFiles("*.*", SearchOption.AllDirectories);
        foreach (var mediaFile in mediaFiles)
        {
            fileCountProcessed++;
            Console.WriteLine($"Processing File: {fileCountProcessed} of {fileCountTotal} {mediaFile}");
            if (filenamesToSkip.Exists(x => x == mediaFile.Name) )
            {
                Console.WriteLine($"Skipping file: {mediaFile}");
                continue;
            }
            try
            {
                IngestMedia(mediaFile.FullName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
    }

    public static bool IngestMedia(string filePath)
    {
        try
        {
            var mediaFile = new FileInfo(filePath);
            Media media = new Media(mediaFile);
            var serializeOptions = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            MediaDb.AddMedia(media).Wait();  //TODO: Remove this .wait()
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return false;
        }

        return true;
    }
    
    public static int GetFileCount(string folderPath)
    {
        DirectoryInfo mediaDir = new DirectoryInfo(folderPath);
        int fileCountTotal = mediaDir.GetFiles("*.*", SearchOption.AllDirectories).Length;
        Console.WriteLine($"Found {fileCountTotal} files");
        return fileCountTotal;
    }


    
}
namespace MediaToolLib.Models;

public class AudioStream
{
    public TimeSpan Duration { get; set; }
    public long BitRate { get; set; }
    public int SampleRate { get; set; }
    public int Channels { get; set; }
}
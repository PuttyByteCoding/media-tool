using FlacLibSharp;
using MetadataExtractor;
using MetadataExtractor.Util;
using Directory = System.IO.Directory;

namespace MediaToolLib.Models;
using System.Security.Cryptography;
using Xabe.FFmpeg;

public class Media
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public string Md5 { get; set; }
    public string FilePath { get; set; }
    public List<VideoStream> VideoStreams { get; set; } = new List<VideoStream>();
    public List<AudioStream> AudioStreams { get; set; } = new List<AudioStream>();
    public List<SubtitleStream> SubtitleStreams { get; set; } = new List<SubtitleStream>();
    public int VideoStreamCount { get; set; }
    public int AudioStreamCount { get; set; }
    public int SubtitleStreamCount { get; set; }
    public string ProcessingNotes { get; set; }
    public List<Metadata> FileMetadata { get; set; } = new List<Metadata>();

    public MediaTypeEnum MediaType;
    public enum MediaTypeEnum
    {
        Audio,
        Video,
        Unknown,
    }
    
    
    public Media(FileInfo mediaFilePath)
    {
        FilePath = mediaFilePath.ToString();
        CalculateMd5Hash();
        try
        {
            GrabMediaMetadata().Wait();
        }
        catch (Exception e)
        {
            ProcessingNotes = e.Message;
        }
        try
        {
            DetermineFileType().Wait();
        }
        catch (Exception e)
        {
            ProcessingNotes = e.Message;
        }
        try
        {
            ReadTags().Wait();
        }
        catch (Exception e)
        {
            ProcessingNotes = e.Message;
        }
        
    }

    public void CalculateMd5Hash()
    {
        byte[] result; 
        MD5 md5 = new MD5CryptoServiceProvider(); 
        using(FileStream fs = new FileInfo(FilePath).OpenRead())
        {
            result = md5.ComputeHash(fs);
        }
        Md5 = BitConverter.ToString(result).Replace("-", "").ToLower();
    }

    public async Task GrabMediaMetadata()
    {
        var mediaMetadata = await FFmpeg.GetMediaInfo(FilePath);
        
        VideoStreamCount = mediaMetadata.VideoStreams.Count();
        foreach (IVideoStream vidStream in mediaMetadata.VideoStreams)
        {
            var videoStream = new VideoStream()
            {
                Duration = vidStream.Duration,
                Height = vidStream.Height,
                Width = vidStream.Width,
                Codec = vidStream.Codec,
                FrameRate = vidStream.Framerate,
                Bitrate = vidStream.Bitrate,
                Ratio = vidStream.Ratio,
                PixelFormat = vidStream.Codec
            };
            
            if (vidStream.Height > vidStream.Width)
            {
                videoStream.IsVertical = true;
            }
            
            VideoStreams.Add(videoStream);
            
        }
        
        AudioStreamCount = mediaMetadata.AudioStreams.Count();
        foreach (IAudioStream audStream in mediaMetadata.AudioStreams)
        {
            var audioStream = new AudioStream()
            {
                Duration = audStream.Duration,
                BitRate = audStream.Bitrate,
                SampleRate = audStream.SampleRate,
                Channels = audStream.Channels
            };
            AudioStreams.Add(audioStream);
        }
        
        SubtitleStreamCount = mediaMetadata.SubtitleStreams.Count();
        foreach (ISubtitleStream subStream in mediaMetadata.SubtitleStreams)
        {
            var subtitleStream = new SubtitleStream()
            {
                Language = subStream.Language,
                Title = subStream.Title
            };
            SubtitleStreams.Add(subtitleStream);
        }
        
        
    }
    
    private async Task ReadTags()
    {
        try
        {
            IReadOnlyList<MetadataExtractor.Directory> directories = ImageMetadataReader.ReadMetadata(FilePath);
            foreach (var directory in directories)
            {
                foreach (var tag in directory.Tags)
                {
                    FileMetadata.Add(new Metadata($"MetadataExtractor {directory.Name}", 
                        tag.Name, 
                        tag.Description));
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        
        var fileExtension = new FileInfo(FilePath).Extension.ToLower();
        
        if (fileExtension == ".flac")
        {
            Console.WriteLine($"Found a FLAC file");
            using (FlacFile file = new FlacFile(FilePath))
            {
                var vorbisComment = file.VorbisComment;
                if (vorbisComment != null)
                {
                    Console.WriteLine($"Vorbis Comments:");
                    foreach (var comment in vorbisComment)
                    {
                        FileMetadata.Add(new Metadata("FLAC Vorbis", 
                            comment.Key, 
                            comment.Value.ToString()));
                    }
                }
            }
        }
    }

    private async Task DetermineFileType()
    {
        var videoFileExtensions = new List<string>()
        {
            ".mp4", ".m4v", ".mov", ".avi"
        };

        var audioFileExtensions = new List<string>()
        {
            ".flac", ".mp3", ".wav", ".ra"
        };

        var fileExtension = new FileInfo(FilePath).Extension.ToLower();
        if (videoFileExtensions.Contains(fileExtension))
        {
            MediaType = MediaTypeEnum.Video;
        }
        else if (audioFileExtensions.Contains(fileExtension))
        {
            MediaType = MediaTypeEnum.Audio;
        }
        else
        {
            MediaType = MediaTypeEnum.Unknown;
        }
        
    }
}
namespace MediaToolLib.Models;

public class VideoStream
{
    public int Height { get; set; }
    public int Width { get; set; }
    public bool IsVertical { get; set; } = false;
    public string Codec { get; set; }
    public double FrameRate { get; set; }
    public long Bitrate { get; set; }
    public string Ratio { get; set; }
    public string PixelFormat { get; set; }
    public TimeSpan Duration { get; set; }
}
namespace MediaToolLib.Models;

public class SubtitleStream
{
    public string Language { get; set; }
    public string Title { get; set; }
}
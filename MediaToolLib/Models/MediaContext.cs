using Microsoft.EntityFrameworkCore;

namespace MediaToolLib.Models;

public class MediaContext: DbContext
{
    public MediaContext(DbContextOptions<MediaContext> options) : base()
    {
    }

    public DbSet<Media> MediaItems { get; set; } = null!;
}
namespace MediaToolLib.Models;

/// <summary>
/// Source: Where the metadata came from (Vorbis Tag, EXIF, Etc) 
/// </summary>
public class Metadata
{
    public string Source { get; set; }
    public string Key { get; set; }
    public string Value { get; set; }
    
    public Metadata(string source, string key, string value)
    {
        Source = source;
        Key = key;
        Value = value;
    }
}
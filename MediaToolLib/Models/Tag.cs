namespace MediaToolLib.Models;

public class Tag
{
    public Guid Id { get; set; } = new Guid();
    public string TagName  { get; set; }
    public string ParentTag  { get; set; }
    public int count { get; set; }
}
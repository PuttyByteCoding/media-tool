﻿using System.Text.Json;
using Elasticsearch.Net;
using Nest;
using MediaToolLib.Models;

namespace MediaToolLib.Database;

public class MediaDb
{
    public static ElasticClient elasticClient { get; set; }
    public static string mediaIndexName { get; set; } = "media";
    public static string tagsIndexName { get; set; } = "tags";
    public static int maxResultsCount { get; set; } = 10000;

    public MediaDb()
    {
        // Get the current fingerprint by going to https://localhost:9200, and checking the cert
        var pool = new SingleNodeConnectionPool(new Uri("https://localhost:9200"));
        var fingerprint = "78 10 11 7D 9B 3C 81 DF C8 B4 FC BB A7 E5 96 8E EA F8 40 B5 F8 42 B3 72 6F 39 5D 12 F9 82 21 1D".Replace(" ", "");
        var elasticUser = "elastic";
        var elasticPass = "elasticpass";
        
        var settings = new ConnectionSettings(pool)
            .CertificateFingerprint(fingerprint)
            .BasicAuthentication(elasticUser, elasticPass)
            .EnableDebugMode()
            .PrettyJson()
            .EnableApiVersioningHeader();

        elasticClient = new ElasticClient(settings);
    }
    
    /// <summary>
    /// Not used.
    /// I thought I needed this to created nested objects, but I do not.
    /// </summary>
    public static async void CreateMappings()
    {
        // Create the mapping
        try
        {
            var createIndexResponse = elasticClient.Indices.Create("media", c => c
                .Map<Media>(m => m
                    .Properties(p => p
                        .Nested<AudioStream>( a => a
                            .Name(na => na.AudioStreams)
                        )
                        .Nested<VideoStream>(v => v
                            .Name(vs => vs.VideoStreams)
                        )
                        .Nested<SubtitleStream>(s => s
                            .Name(ss => ss.SubtitleStreams)
                        )
                        .Nested<Metadata>(md => md
                            .Name(fmd => fmd.FileMetadata))
                    )
                )
            );
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    } 
    
    
    public static async Task<bool> AddMedia(Media media)
    {
        if (HasMd5BeenProcessed(media.Md5))
        {
            Console.WriteLine($"Media: {media.Md5} has already been processed.");
            return false;
        }
        var response = await elasticClient.IndexAsync(
            media, request => 
                request.Index(mediaIndexName)
        );

        if (response.IsValid)
        {
            Console.WriteLine($"Media: {media.Md5} was added.");
        }
        else
        {
            Console.WriteLine($"Something went wrong.");
            Console.WriteLine($"{response.DebugInformation}");
            return false;
        }

        return true;
    }

    public static IReadOnlyCollection<Media>? GrabVideos(int? count)
    {
        int resultsCount = count ?? maxResultsCount;
        Console.WriteLine($"Count limit is: {count}");

        var response = elasticClient.Search<Media>(s => s
            .Index(mediaIndexName)
            .Size(resultsCount)
            .Query(q => q
                .Term(t => t.MediaType, 1)                
            )
        );
       
        // var query = $@"
        // {{
        //   ""size"": {resultsCount},
        //   ""query"": {{
        //     ""match"": {{
        //       ""mediaTyp"": ""1""
        //       }}
        //     }}
        //   }}
        // }}
        // ";
        //
        // var response = elasticClient.LowLevel.Search<SearchResponse<Media>>(query);

        if (response.IsValid)
        {
            if (response.Documents.Count >= maxResultsCount)
            {
                Console.WriteLine($"Results count is at Max results count ({maxResultsCount}).  You may be missing items ");
            }
            return response.Documents;            
        }
        return null;
        
    }

    public static bool HasMd5BeenProcessed(string md5Value)
    {
        var response = elasticClient.Search<Media>(s => s
            .Index(mediaIndexName)
            .Size(1)
            .Query(q => q
                .Term(t => t.Md5, md5Value)
            )
        );

        if (response.IsValid && (response.Documents.Count > 0))
        {
                return true;
        }
        return false;
    }

    public static IReadOnlyCollection<Media>? GrabMediaByMd5(string md5Value)
    {
        var response = elasticClient.Search<Media>(s => s
            .Index(mediaIndexName)
            .Size(maxResultsCount)
            .Query(q => q
                .Term(t => t.Md5, md5Value)
            )
        );
        
        if (response.IsValid)
        {
            if (response.Documents.Count >= maxResultsCount)
            {
                Console.WriteLine($"Results count is at Max results count ({maxResultsCount}).  You may be missing items ");
            }
            return response.Documents;            
        }
        return null;
    }
    
    public static IReadOnlyCollection<Media>? GrabMediaByAudioStreamSampleRate(long sampleRate)
    {
        var query = $@"
        {{
          ""query"": {{
            ""match"": {{
              ""audioStreams.duration"": ""{sampleRate}""
            }}
          }}
        }}
        ";

        var response = elasticClient.LowLevel.Search<SearchResponse<Media>>(query);
        
        
        /////// This works:
        // GET media/_search
        // {
        //     "query": {
        //         "match": {
        //             "audioStreams.duration": "5214498129"
        //         }
        //     }
        //
        // }
        ////// why does this not work?
        // https://www.elastic.co/guide/en/elasticsearch/client/net-api/7.17/writing-queries.html
        // var response = elasticClient.Search<Media>(s => s
        //     .Index("media")
        //         .Query(q => q
        //             .Match(m => m
        //                 .Field(f => f.AudioStreams.sampleRate)
        //                 .Query("2")
        //             )
        //         )
        //     );
        
    
        if (response.IsValid)
        {
            if (response.Documents.Count >= maxResultsCount)
            {
                Console.WriteLine($"Results count is at Max results count ({maxResultsCount}).  You may be missing items ");
            }
            return response.Documents;            
        }
        return null;
    }

    public static IReadOnlyCollection<Media> GrabMediaByMetadataKey(string metadataKey)
    {
        // I was using this to test nested search because there are more items in this property
        // Does not work as expected
        throw new NotImplementedException();
        var response = elasticClient.Search<Media>(s => s
            .Index("media")
            .Query(q => q
                .Nested(n => n
                    .Path(p => p.FileMetadata)
                    .Query(nq => nq
                        .Terms(t => t
                            .Field(f => f.FileMetadata.First().Source)
                            .Terms(metadataKey)
                        )
                    )
                )
            )
        );
    
        if (response.IsValid)
        {
            if (response.Documents.Count >= maxResultsCount)
            {
                Console.WriteLine($"Results count is at Max results count ({maxResultsCount}).  You may be missing items ");
            }
            return response.Documents;            
        }
        return null;
    }
    
    public static IReadOnlyCollection<Media>? GrabMediaByVideoStreamCount(int vidStreamCount)
    {
        
        var response = elasticClient.Search<Media>(s => s
            .Index(mediaIndexName)
            .Size(maxResultsCount)
            .Query(q => q
                .Term(t => t.VideoStreamCount, vidStreamCount)
            )
        );
        
        if (response.IsValid)
        {
            if (response.Documents.Count >= maxResultsCount)
            {
                Console.WriteLine($"Results count is at Max results count ({maxResultsCount}).  You may be missing items ");
            }
            return response.Documents;            
        }
        return null;
    }
    
    public static TermsAggregate<string> GrabHeightAggregates()
    {
        var response = elasticClient.Search<Media>(s => s
            .Index(mediaIndexName)
            .Aggregations(a => a
                .Terms("height", t => t
                    .Field("height")
                    .MinimumDocumentCount(1)
                )
            )
        );
        
        if (!response.IsValid)
        {
            throw new Exception();
        }
        
        var terms = response.Aggregations.Terms("height");
        return terms;
    }
    
    public static TermsAggregate<string> GrabStreamCountAggregates()
    {
        var response = elasticClient.Search<Media>(s => s
            .Index(mediaIndexName)
            .Aggregations(a => a
                .Terms("video_stream_count", t => t
                    .Field("video_stream_count")
                    .MinimumDocumentCount(1)
                )
            )
        );
        
        if (!response.IsValid)
        {
            throw new Exception();
        }
        
        var terms = response.Aggregations.Terms("video_stream_count");
        return terms;
    }
    

    public static void PrintIndexNames()
    {
        Console.WriteLine($"Grabbing index names...");
        var indicesResults = MediaDb.elasticClient.Indices.Get(Indices.All);

        if (indicesResults.IsValid)
        {
            foreach (var index in indicesResults.Indices)
            {
                var indexName = index.Key.ToString();
                Console.WriteLine($"Index found: {indexName}");
            }

            var c = elasticClient.Indices.Stats();
            Console.WriteLine($"Index Count is: {c.Indices.Count}");
        }
        else
        {
            Console.WriteLine($"Error grabbing indexes {indicesResults.DebugInformation}");
        }
        
    }
    

}
﻿using System.Text.Json;
using MediaToolLib.Database;
using MediaToolLib.Ingestion;
using MediaToolLib.Analysis;

var mediaDb = new MediaDb();

Ingest.IngestFromFolder("/Volumes/RAID/__TEMP");
// MediaDb.CreateMappings();

var a = MediaDb.GrabMediaByAudioStreamSampleRate(5214498129);


// var a = MediaDb.GrabMediaByAudioStreamSampleRate(44100);
// var b = MediaDb.GrabMediaByMetadataKey("MetadataExtractor AVI");

// Print Indexes
// MediaDb.PrintIndexNames();

var serializeOptions = new JsonSerializerOptions
{
    WriteIndented = true
};



// // TODO: Make this a test
// var videosWithHeight = MediaDb.GrabVideosByHeight("1080");
// var serializeJsonHeight = JsonSerializer.Serialize(videosWithHeight, serializeOptions);
// Console.WriteLine(serializeJsonHeight);


// // TODO: Make this a test
// var videosWithMD5 = MediaDb.GrabVideoByMd5("92b7645bc10cb1f7f5aeae0b96be4465");
// var serializedJsonMD5s = JsonSerializer.Serialize(videosWithMD5, serializzeOptions);
// Console.WriteLine(serializedJsonMD5s);


// var videoAnalysis = new Analysis();
// videoAnalysis.VideosStreamsReport();
// videoAnalysis.VideoHeightReport();
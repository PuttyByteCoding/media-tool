# Media Tool
* Media = Video and Audio.  I'm not doing anything with photos.  Yet.

## Create/Configure docker container

## Starting Elastic docker container
* Docker files are in `.\ElasticDocker` folder in the root of this repo
* Username and password are define in the `` file
* `docker-compose up -d` - Starts the ElasticSearch container
    * `-d` Detached Mode. Run containers in the background
* `docker-compose down` - Stopped the ElasticSearch container
    * Data in the docker volumes is preserved
* `docker-compose down -v` - Resets ElasticSearch container.
    * `-v`  Remove named volumes declared in the volumes section of the Compose file.
    * Data is removed.

## Update
* Go to
* View the certificate
* Fingerprints - SHA-256

## Pages
* `https://localhost:9200` - Elastic.  Returns JSON.
* `http://localhost:5601` - Kabana

